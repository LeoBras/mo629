import json
import time
import numpy as np
import cv2
import requests
count = 0

captura = cv2.VideoCapture(5)

url = 'https://api.tago.io/data'

headers = {
    # token do divece: TV_box_Contar_Pessoas
    'device-token': '54ac8055-f73d-4a5b-bc76-70f83663e3db',
    'Content-Type': 'application/json'
}

def count_people():
    global count
    ret, frame = captura.read()

    count = count + 1 
    if count == 50:
        count = 0
        cascade = cv2.CascadeClassifier('haarcascade_frontalface.xml');

        img = cv2.resize(frame, (600, 480))
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY);

        body = cascade.detectMultiScale(
            gray,
            scaleFactor = 1.05,
            minNeighbors = 5,
            minSize = (30,30),
            flags = cv2.CASCADE_SCALE_IMAGE
        )

        for (x, y, w, h) in body:
            cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)

        num_of_people = len(body)

        print(num_of_people)

        data = [
            {
                "variable": "quantidadePessoas",
                "value":num_of_people,
                "unit": 'int'
            }
        ]

        response = requests.post(url, headers=headers, data=json.dumps(data))

        cv2.imshow("Image", img)

        if response.status_code in [200,202]:
            print(f'Request successful with code {response.status_code}. Response:')
            print(response.json())
            print(json.dumps(response.json(), indent=2))
        else:
            print(f'Request failed with status code {response.status_code}. Response:')
            print(response.text)

while(1):
    
    count_people()

    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

captura.release()
cv2.destroyAllWindows()
