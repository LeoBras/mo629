import requests
import json
import random

random_quantidd_pessoas = random.randint(0,60)

url = 'https://api.tago.io/data'
headers = {
    # token do divece: TV_box_Contar_Pessoas
    'device-token': '54ac8055-f73d-4a5b-bc76-70f83663e3db',
    'Content-Type': 'application/json'
}

data = [{
        "variable": "quantidadePessoas",
        "value":random_quantidd_pessoas,
        "unit": 'int'
        }
       ]



response = requests.post(url, headers=headers, data=json.dumps(data))

if response.status_code in [200,202]:
    print(f'Request successful with code {response.status_code}. Response:')
    print(response.json())
    print(json.dumps(response.json(), indent=2))
else:
    print(f'Request failed with status code {response.status_code}. Response:')
    print(response.text)
