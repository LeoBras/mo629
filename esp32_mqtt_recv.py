import requests
import random
import json
import time

# imports micropython
# import machine
# import urequests as requests
# import ujson as json
# import utime as time
# import urandom as random

# Configuração do pino GPIO para o relé
# relay_pin = machine.Pin(23, machine.Pin.OUT)


lightON = True
lightOFF = False

airConditionerON = True
airConditionerOFF = False
airConditionerIsON = False

requestinterval = 5


url = 'https://api.tago.io/data'

def getDataFromBroker():
  headersGet = {
    # token do divece: TV_box_Contar_Pessoas
    'device-token': '54ac8055-f73d-4a5b-bc76-70f83663e3db',
  }

  params = {
  'query': 'last_item',
  }
  response = requests.get(url, headers=headersGet, params=params)
  return response

def sendDataToBroker(temperature):
  # random_temperature = random.randint(10,30)
  headersPost = {
      # token do divece: TV_box_Contar_Pessoas
      'device-token': '54ac8055-f73d-4a5b-bc76-70f83663e3db',
      'Content-Type': 'application/json'
  }

  dataPost = [{
          "variable": "temperature",
          "value":temperature,
          "unit": 'ºC'
          }
        ]

  response = requests.post(url, headers=headers, data=json.dumps(data))

  if response.status_code in [200,202]:
      print(f'Request successful with code {response.status_code}. Response:')
      print(response.json())
      print(json.dumps(response.json(), indent=2))
      return True
  else:
      print(f'Request failed with status code {response.status_code}. Response:')
      print(response.text)
      return False



def amountPeopleVerification(responseData):
  if ("value" in responseData.keys()):
    return responseData["value"]
  else:
    print("VALOR DAS PESSOAS NÃO PRESENTE NA RESPOSTA")
    return -1


def turnOnOffLight(varTurnOnOffLight):
  if (varTurnOnOffLight):
    print('LIGANDO O RELÈ')
    print("LIGANDO A LUZ")
    # relay_pin.on()
  else:
    print('DESLIGANDO O RELÈ')
    print("DESLIGANDO A LUZ")
    # relay_pin.off()



def analysingRoomConditions(responseData, peopleThreshold, temperatureThreshold):
  people = amountPeopleVerification(responseData)
  temperature = checkTemperature()

  print(f"PESSOAS = {people}")
  # A quantidade de pessoas é maior que minimo, liga a luz
  if (people >= peopleThreshold):
    turnOnOffLight(lightON)

    # agora que tem pessoas e luz foi ligada, a temperatura esta maior que minimo? Se sim, liga o ar
    if (temperature >= temperatureThreshold):
      turnOnOffAirConditioner(airConditionerON)
      sendDataToBroker(temperature)
  else:
    # não tem pessoas? Desliga o ar e a luz
    turnOnOffLight(lightOFF)
    turnOnOffAirConditioner(airConditionerOFF)




# Verifica se "é para ligar o AR  'E' o ar está desligado"
def turnOnOffAirConditioner(varTurnOnOffAirConditioner):
  global airConditionerIsON

  # é para ligar o ar
  if varTurnOnOffAirConditioner:
    # o ar está deslisgado?
    if not airConditionerIsON:
      print("ENVIANDO SINAL DE INFRAVERMELHO")
      print("LIGANDO O AR CONDICIONADO")
      airConditionerIsON = True
  else:
    # quando cai aqui é porque é para desligar o AR
    print("ENVIANDO SINAL DE INFRAVERMELHO")
    print("DESLIGANDO A LUZ")
    airConditionerIsON = False


def checkTemperature():
  random_temperatura = random.randint(0,40)
  print('LENDO A TEMPERATURA......')
  print(f'TEMPERATURA = {random_temperatura}')
  return random_temperatura







while True:
  response = getDataFromBroker()

  if response.status_code in [200,202]:
      # print('Request successful. Response:')
      # print(response.json())
      # print(json.dumps(response.json(), indent=2))

      data = response.json()
      if data['status']:
          peopleThreshold = 50
          temperatureThreshold = 20
          result_dict = data['result'][0]
          print(f'Request successful with code {response.status_code}. Response:')
          print(result_dict)
          print(json.dumps(data, indent=2))

          analysingRoomConditions(result_dict, peopleThreshold, temperatureThreshold)

      else:
          print('Request was successful, but "status" is false.')



  else:
      print(f'Request failed with status code {response.status_code}. Response:')
      print(response.text)

  print()
  print('*'*50)
  print()
  time.sleep(requestinterval)
